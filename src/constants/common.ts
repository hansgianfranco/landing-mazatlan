export const apiUrl = process.env.API_URL || 'https://kvjzaug6ec.execute-api.us-east-2.amazonaws.com/dev';

// @ts-ignore
export const fetcher = (...args) => fetch(...args).then((res) => res.json());
