export type carousel = {
  id: string;
  image_url: string;
  title_es: string;
  title_en: string;
  abstract_es: string;
  abstract_en: string;
  content_es: string;
  content_en: string;
};

type team = {
  shield_url?: string;
};

type score = {
  home_score?: string;
  away_score?: string;
};

export type matches = {
  id: string;
  home_team: team;
  away_team: team;
  title: string;
  date: string;
  score: score;
};

export type img = {
  id: string;
  web_view_url: string;
  image_url: string;
};

export type member = {
  id: string;
  name: string;
  last_name: string
  shirt: string
  position: string
  image_url: string | null;
};
