import Image from "next/image";

export const LogoBanners: React.FC = () => {
  return (
    <div className="hidden md:block bg-black py-2">
      <div className="max-w-[1290px] mx-auto">
        <div className="relative h-[15px] sm:h-[20px] lg:h-[28px]">
          <Image 
            fill 
            alt="logos" 
            sizes="80vw"
            objectFit="cover"
            src="/logos/logos.png" 
          />
        </div>
      </div>
    </div>
  );
};
