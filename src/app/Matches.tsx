"use client";

import useSWR from "swr";
import Image from "next/image";
import type { matches } from "@/constants";
import { apiUrl, fetcher } from "@/constants";
import Slider from "react-slick";
import moment from 'moment';
import 'moment/locale/es';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./custom-slick.css";


export const Matches: React.FC<{ showOverlay: React.Dispatch<React.SetStateAction<boolean>>, selectedMatch: React.Dispatch<React.SetStateAction<any>> }> = ({ showOverlay, selectedMatch }) => {

  const slickSettings = {
    dots: false,
    arrows: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1
  }

  const {
    data: currentMatchesData,
    error: upcomingError,
    isLoading: upcomingLoading,
  } = useSWR(`${apiUrl}/api/matches/latest`, fetcher);

  const {
    data: upcomingMatchesData,
    error: currentError,
    isLoading: currentLoading,
  } = useSWR(`${apiUrl}/api/matches/next`, fetcher);

  const matches = [
    currentMatchesData?.data,
    ...(upcomingMatchesData?.data ?? []).map((match: matches) => ({
      ...match,
      isDisabled: true,
    })),
  ];

  const formateDate = (date: string) => {
    return moment(date).locale('es').format('dddd DD/MM/YYYY, HH:mm');
  };

  const getProgress = (time: number) => {
    let step = 0;
    switch (true) {
      case time >= 90:
        step = 8;
        break;
      case time >= 80:
        step = 7;
        break;
      case time >= 70:
        step = 6;
        break;
      case time >= 60:
        step = 5;
        break;
      case time >= 50:
        step = 4;
        break;
      case time >= 40:
        step = 3;
        break;
      case time >= 30:
        step = 2;
        break;
      case time >= 20:
        step = 1;
        break;
      case time >= 10:
        step = 0;
        break;
      default:
        step = 0;
        break;
    }
    return `/steps/${step}.svg`;
  };


  if (upcomingError || currentError) return null;
  if (upcomingLoading || currentLoading) return "";

  return (
    <div id="proximos_partidos" className="mb-[50px] md:mb-[120px]">
      <div className="relative px-[30px] md:before:absolute md:before:h-full md:before:w-[1px] md:before:bg-white md:before:left-[30px] before:top-[20px]">
        <div className="py-[12px] md:py-[30px] md:ml-[26px] ">
          <div className="text-[18px] font-bold md:text-[36px] leading-[30px]">PRÓXIMOS PARTIDOS</div>
        </div>
        <div className="md:mx-[15px]">
          <div className="slick-custom-1">
            <Slider {...slickSettings}>
              {matches.length > 0 && matches.map((match) => {
                return (
                  <button
                    key={match.id}
                    className="px-[10px]"
                    onClick={() => {
                      selectedMatch(match);
                      showOverlay(true);
                    }}
                  >
                    <div className="w-[276px] h-[191px] md:w-[450px] md:h-[316px] relative rounded-2xl duration-100 bourded-[16px]">
                      <div
                        className="absolute bottom-0 top-0 left-0 right-0 z-[10]"
                        style={{
                          background:
                            "linear-gradient(0deg, rgba(0,0,0,0.9318102240896359) 0%, rgba(0,0,0,0) 60%);",
                        }}
                      />

                      <Image
                        fill
                        sizes="100%"
                        alt="match"
                        src="/teams/bg1.jpeg"
                        className={match.isDisabled ? "grayscale" : ""}
                        style={{
                          objectFit: "cover",
                          borderRadius: "16px",
                        }}
                      />

                      <div className="absolute top-[35px] md:top-[58px] left-0 right-0 px-[23px] md:px-[30px] flex items-start justify-between">
                        {match.home_team?.shield_url ? (
                          <div className="max-w-[110px] w-full">
                            <div className="relative mx-auto h-[51px] w-[51px] md:h-[85px] md:w-[85px]">
                              <Image
                                fill
                                sizes="100%"
                                alt="carousels"
                                src={match.home_team.shield_url}
                                style={{
                                  objectFit: "cover",
                                }}
                              />
                            </div>
                            <div className="font-bold text-center text-[8px] md:text-[14px] leading-[27px]">
                              {match.home_team?.name ?? "-"}
                            </div>
                            <div className="text-[#0FC8E4] text-center text-[6px] md:text-[10px] leading-[16px]">Local</div>
                          </div>
                        ) : null}

                        <div className="relative h-[84px] w-[84px] md:h-[140px] md:w-[140px]">
                          <div className="relative h-[84px] w-[84px] md:h-[140px] md:w-[140px]">
                            <Image
                              fill
                              sizes="100%"
                              alt="carousels"
                              src={
                                match.isDisabled
                                  ? "/commons/progress-empty.svg"
                                  : getProgress(match.score?.update_minute)
                              }
                              style={{
                                objectFit: "cover",
                              }}
                            />
                          </div>
                          <div className="absolute top-[22px] md:top-[36px] left-0 right-0 text-center text-[8px] md:text-sm z-[10]">
                            {(match.score?.update_minute ?? "-") + "’"}
                          </div>
                          <div className="absolute top-0 left-0 right-0 bottom-0 m-auto h-[40px] text-center text-[18px] md:text-[30px] leading-[45px] z-[10] font-bold">
                            {match.score?.home_score ?? "0"} - {" "}
                            {match.score?.away_score ?? "0"}
                          </div>
                        </div>

                        {match.away_team?.shield_url ? (
                          <div className="max-w-[110px] w-full">
                            <div className="relative mx-auto h-[51px] w-[51px] md:h-[85px] md:w-[85px]">
                              <Image
                                fill
                                sizes="100%"
                                alt="carousels"
                                src={match.away_team.shield_url}
                                style={{
                                  objectFit: "cover",
                                }}
                              />
                            </div>
                            <div className="font-bold text-center text-[8px] md:text-[14px] leading-[27px]">
                              {match.away_team?.name ?? "-"}
                            </div>
                            <div className="text-[#0FC8E4] text-center text-[6px] md:text-[10px] leading-[16px]">Visita</div>
                          </div>
                        ) : null}
                      </div>

                      <div className="absolute bottom-[22px] left-0 right-0 text-center z-[1]">
                        <p className="uppercase text-[11px] md:text-[18px] leading-[20px] md:leading-[27px] font-bold">ESTADIO {match.stadium ?? "-"}</p>
                        <p className="capitalize text-[8px] md:text-[12px] leading-[11px] md:leading-[16px]">{formateDate(match.date + " " + match.time)}</p>
                      </div>
                    </div>
                  </button>
                );
              })}
            </Slider>
          </div>
        </div>
      </div>
    </div>
  );
};
