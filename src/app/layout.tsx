import type {Metadata} from "next";
import {Poppins, Inter} from "next/font/google";
import localFont from 'next/font/local'
import "./globals.css";

const poppins = Poppins({
  display: 'swap',
  style: ["normal"],
  subsets: ["latin"],
  weight: ["400", "600", "700"],
  variable: '--font-poppins'
})
const inter = Inter({
  display: 'swap',
  style: ["normal"],
  subsets: ["latin"],
  weight: ["700"],
  variable: '--font-inter'
})
const arialNarrow = localFont({ 
  src: '../fonts/Arial-Narrow-Font.ttf', 
  variable: '--font-arial-narrow' 
})

export const metadata: Metadata = {
  title: "Mazatlán FC",
  description: "",
};

export { poppins, inter, arialNarrow }

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${poppins.variable} ${inter.variable} ${arialNarrow.variable}`}>{children}</body>
    </html>
  );
}
