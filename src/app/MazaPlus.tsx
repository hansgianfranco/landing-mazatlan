import Image from "next/image";
import Link from "next/link";

export const MazaPlus: React.FC = () => {
  return (
    <div className="relative w-[80vw] mx-auto mb-[57px] md:mb-[120px] rounded-[8px] md:rounded-[16px] overflow-hidden">
        <Link href={"https://www.youtube.com/watch?v=ZgjGM-j15Rg"} target="_blank">
        <Image
          fill
          sizes="100%"
          alt="carousels"
          className="!relative !h-auto"
          src="/commons/nav4.png"
        />
        </Link>
    </div>
  );
};
